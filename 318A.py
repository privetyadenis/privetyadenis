import sys

local_launch = False
try:
    open('LOCAL').read()
    local_launch = True
except:
    pass
input_data = open('INPUT.txt').read() if local_launch else sys.stdin.read()
done = {'i': 0}
tokens = input_data.split()

def next_token():
    result = tokens[done['i']]
    done['i'] += 1
    return result

def next_int():
    return int(next_token())

def next_str():
    return next_token()

############## BEGIN OF CODE #####################
n = next_int()
k = next_int()
x = (n+1)//2
if k <= k:
    print(k * 2 - 1)
else:
    print((k-x) * 2)

