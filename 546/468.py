import sys

local_launch = False
try:
    open('LOCAL').read()
    local_launch = True
except:
    pass
input_data = open('INPUT.txt').read() if local_launch else sys.stdin.read()
done = {'i': 0}
tokens = input_data.split()

def next_token():
    result = tokens[done['i']]
    done['i'] += 1
    return result

def next_int():
    return int(next_token())

def next_str():
    return next_token()

############## BEGIN OF CODE #####################

n = next_int()


if n > 0:
    print(n)
else:
    n = n * - 1
    y = n % 10
    u = (n // 10) % 10
    if y > u:
        print((n // 10) * - 1)

    else:
        print((n // 100 * 10 + n % 10) * - 1)











############# END OF CODE #####################