import sys

local_launch = False
try:
    open('LOCAL').read()
    local_launch = True
except:
    pass
input_data = open('INPUT.txt').read() if local_launch else sys.stdin.read()
done = {'i': 0}
tokens = input_data.split()

def next_token():
    result = tokens[done['i']]
    done['i'] += 1
    return result

def next_int():
    return int(next_token())

def next_str():
    return next_token()



def index(a, x):
    for i in range(len(a)):
        if a[i] == x:
            return i

############## BEGIN OF CODE #####################
n = next_int()
a = []
odd = 0
even = 0
for i in range(n):
    x = next_int()
    a.append(x)

for i in range(len(a)):
    if a[i] % 2 != 0:
        odd += 1

for i in range(len(a)):
    if a[i] % 2 == 0:
        even += 1
if odd>even:
    for i in range (len(a)):
        if a[i] %2 == 0:
            print(i+1)
if even > odd:
    for i in range(len(a)):
        if a[i] % 2 != 0:
            print(i+1)

