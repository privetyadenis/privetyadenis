import sys

local_launch = False
try:
    open('LOCAL').read()
    local_launch = True
except:
    pass

input_data = open('INPUT.txt').read() if local_launch else sys.stdin.read()
done = {'i': 0}
tokens = input_data.split()

def next_token():
    result = tokens[done['i']]
    done['i'] += 1
    return result

def next_int():
    return int(next_token())

def next_str():
    return next_token()

############## BEGIN OF CODE #####################
def sum(a):
    z = 0
    for i in range(len(a)):
        z = z + a[i]
    return z

def max(a):
    maz = a[0]
    for i in range(len(a)):
        if maz < a[i]:
                maz = a[i]
    return maz

def min(a):
    mix = a[0]
    for i in range(len(a)):
        if mix > a[i]:
                mix = a[i]
    return mix

def argmax(a):
    id = 0
    maz = a[0]
    for i in range(len(a)):
        if maz < a[i]:
            maz = a[i]
            id = i
    return id

def argmin(a):
    id = 0
    mix = a[0]
    for i in range(len(a)):
        if mix > a[i]:
            mix = a[i]
            id = i
    return id


def argmax_last(a):
    id = 0
    maz = a[0]
    for i in range(len(a)):
        if maz <= a[i]:
            maz = a[i]
            id = i
    return id

def argmin_last(a):
    id = 0
    mix = a[0]
    for i in range(len(a)):
        if mix >= a[i]:
            mix = a[i]
            id = i
    return id

######################
#n = next_int()
#a = []
#for i in range(n):
      #  x = next_int()
      #  a.append(x)
#print(a)

#count1 = 0
#count = 0
#b=0
#for i in range(argmin_last(a),len(a)-1):
    #меняем i and i +1
  #  b = a[i]
  #  a[i] = a[i + 1]
  #  a[i+1] = b
  #  count += 1

#for i in range(argmax(a),0,-1):
    #меняем i and i-1
 #   b = a[i]
 #   a[i] = a[i - 1]
 #   a[i-1] = b
#    count1+=1


#print(count+count1)



############################ REAL CODE #####################
n = next_int()
def row(n):
    a = []
    for i in range(n):
        a.append(i)


    b = []
    for i in range(n, -1, -1):
        b.append(i)


    c = a + b
    return c

def str_row(b):
    c = ''
    for i in range(len(b)):
        if i != 0:
            c += ' '
        c += str(b[i])
    return c




for i in row(n):
    x = ' ' * (n - i)*2+str_row(row(i))
    print (x)