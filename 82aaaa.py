import sys

local_launch = False
try:
    open('LOCAL').read()
    local_launch = True
except:
    pass

input_data = open('INPUT.txt').read() if local_launch else sys.stdin.read()
done = {'i': 0}
tokens = input_data.split()

def next_token():
    result = tokens[done['i']]
    done['i'] += 1
    return result

def next_int():
    return int(next_token())

def next_str():
    return next_token()

############## BEGIN OF CODE #####################
n = next_int()


def prefix(m):
    s = 0
    part = 5
    count = 0
    while count < m :
        s += part
        part *= 2
        count += 1
    return s

def search(n):
    m = 1
    while prefix(m) <= n:
        m = m + 1

    return m

def people(k):
    return 5 * 2 ** (k - 1)

queue = 'SLPRH'

m = search(n)
u = prefix(m - 1)
j = n - u - 1
p = people(m)
t = p // 5
y = j // t

letter = queue[y]

names = dict()
names['S'] = 'Sheldon'
names['L'] = 'Leonard'
names['P'] = 'Penny'
names['H'] = 'Howard'
names['R'] = 'Rajesh'

print(names[letter])