import sys

local_launch = False
try:
    open('LOCAL').read()
    local_launch = True
except:
    pass
input_data = open('INPUT.txt').read() if local_launch else sys.stdin.read()
done = {'i': 0}
tokens = input_data.split()

def next_token():
    result = tokens[done['i']]
    done['i'] += 1
    return result

def next_int():
    return int(next_token())

def next_str():
    return next_token()

############## BEGIN OF CODE #####################
#Для начала мы должы сделать 3 операции
#1 мы должны поделить наше число U // 4 = 0 то пишим Yes
#1 если не делиться то мы переходим к след мы U // 7 = 0 то пишим Yes
# потом мы проверяем что число должно состоять только из 4 и 7 только если на 4 и на 7 не поделилось
#если нечего не помогло то No
n = next_int()
def get_digits(n):
    # Вход: n - целое число
    # Выход: digits - список цифр числа n
    digits = []
    while(n>0):
            u = (n % 10)
            n = n // 10
            digits.append(u)

    return digits
m = get_digits(n)



def is_happy(n):
    digits = get_digits(n)
    for d in digits:
        if d != 7 and d != 4:
            return False
    return True


def is_good(n):
    for i in range(1,n + 1):
        if is_happy(i):
            if n % i == 0:
                return "YES"

    return "NO"

m = is_good(n)
print(m)









############# END OF CODE #####################