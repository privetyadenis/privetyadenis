import sys

local_launch = False
try:
    open('LOCAL').read()
    local_launch = True
except:
    pass

input_data = open('INPUT.txt').read() if local_launch else sys.stdin.read()
done = {'i': 0}
tokens = input_data.split()

def next_token():
    result = tokens[done['i']]
    done['i'] += 1
    return result

def next_int():
    return int(next_token())

def next_str():
    return next_token()

############## BEGIN OF CODE #####################
def sum(a):
    z = 0
    for i in range(len(a)):
        z = z + a[i]
    return z

def max(a):
    maz = a[0]
    for i in range(len(a)):
        if maz < a[i]:
                maz = a[i]
    return maz

def min(a):
    mix = a[0]
    for i in range(len(a)):
        if mix > a[i]:
                mix = a[i]
    return mix

def argmax(a):
    id = 0
    maz = a[0]
    for i in range(len(a)):
        if maz < a[i]:
            maz = a[i]
            id = i
    return id

def argmin(a):
    id = 0
    mix = a[0]
    for i in range(len(a)):
        if mix > a[i]:
            mix = a[i]
            id = i
    return id


def argmax_last(a):
    id = 0
    maz = a[0]
    for i in range(len(a)):
        if maz <= a[i]:
            maz = a[i]
            id = i
    return id

def argmin_last(a):
    id = 0
    mix = a[0]
    for i in range(len(a)):
        if mix >= a[i]:
            mix = a[i]
            id = i
    return id

######################
#n = next_int()
#a = []
#for i in range(n):
      #  x = next_int()
      #  a.append(x)
#print(a)

#count1 = 0
#count = 0
#b=0
#for i in range(argmin_last(a),len(a)-1):
    #меняем i and i +1
  #  b = a[i]
  #  a[i] = a[i + 1]
  #  a[i+1] = b
  #  count += 1

#for i in range(argmax(a),0,-1):
    #меняем i and i-1
 #   b = a[i]
 #   a[i] = a[i - 1]
 #   a[i-1] = b
#    count1+=1


#print(count+count1)



############################ REAL CODE ###########################
a = next_int()
b = next_int()
c = next_int()
x1 = a + b + c
x2 = a * b + c
x3 = a * b * c
x4 = a + b * c
x5 = (a + b) * c
x6 = a * (b + c )
z = [x1,x2,x3,x4,x5,x6]



print(max(z))